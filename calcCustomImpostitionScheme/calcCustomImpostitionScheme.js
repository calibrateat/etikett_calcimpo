// expected: print PDF (page 1: print content - page 2: die) with XML dataset stanz_xml
// result
// downloaded diePDF with all private variables to swap dataset
// private Variables for
/*
	job.setPrivateData('die.rows',y);
	job.setPrivateData('die.columns',x);
	job.setPrivateData('die.slotWidth',getSlotWidth());
	job.setPrivateData('die.slotHeight',getSlotHeight());
	job.setPrivateData('die.sheetWidth',getSheetWidth());
	job.setPrivateData('die.sheetHeight',getSheetHeight());
	job.setPrivateData('die.customerImpositionSchema',getCustomImpositionSchema());
	job.setPrivateData('die.offcutReight',(x/2));
	job.setPrivateData('die.offcutTop',(y/2));
	job.setPrivateData('die.offcutBottom',(y/2));
	die.toolOriginX - x offset from MediaBox LL
	die.toolOriginY - y offset from MediaBox LL
	die.x
	die.y
	die.mediaStartOffset - x offset of printed media relative to MediaBox LL
	die.bleedLeft|Reight|Bottom|Top
	die.maxSheetLength
	die.textOffsetHorizontal
	die.numberOfFrames
*/

// 180111 - fix - orientation is ignored if slot is a quadradic
// 171006 - enhancement for multiple print items. Every position in of the tool has a separate PDF
// 171102 - Fix: the use of a1 and a2 was wrong and mixed
// 171102 - Enh: support for multiple frames. Change order of pages. First all print items, then the leadIn.
// 171211 - Fix: Default for Slot is not landscape since default for PDFs is portrait. Is an issue if a PDF and a slot is rectangular
// 180102 - Fix: Offcut was not honoring slot rotation when a1 and a2 was different

// Is invoked each time a new job arrives in one of the input folders for the flow element.
// The newly arrived job is passed as the second parameter.

//var _job;
const materialWidth = {min : 50, max : 330};
const materialLength = 980;

// Distance of Text from reight BoundingBox plus Registration Mark in mm
const cTextBoxDistanceReightTopHorizontal = 10.5;
const cMinimalNeededSlug = 6.5;
const cBleedMaxMm = 3;
// Minimal needed SligArea for marks
function jobArrived( s : Switch, job : Job )
{
	//_job = job;
	try {
		var XML = new Document (job.getDataset('stanz_XML').getPath());
		
		// create die Object and pass orienatation of PDF
	
		var _d = new die(XML, (job.getVariableAsNumber('[Stats.MediaBoxHeight]') >= job.getVariableAsNumber('[Stats.MediaBoxWidth]')), s, job );
		
		var numPages = job.getVariableAsNumber('[Stats.NumberOfPages]');
		if (isNaN (numPages)) {
			job.log(3, "Number of pages cannot be read. Maybe job is not a PDF document");
			job.setPrivateData('last.message',  "Number of pages cannot be read. Maybe job is not a PDF document");
			job.setPrivateData('last.severity', "3");
			job.sendToData(3, job.getPath() );
			return;
		}
		
		if (numPages > 2) {
			//if the PDF has ore than one page, then we match it with the x parameter and set numberOfPages
			
			// 1 more than frames due to the leadin Pages
			if (numPages == (_d.x * _d.y * (_d.frames + 1))) {
				//set number of Pages in PDF document
				_d.numberOfPages = numPages;
			} else {
				job.log(3, "Number of pages in PDF does not match number of columns in die. PDF: %11, die: %12, frames: %13", [(numPages / 2), (_d.x * _d.y * (_d.frames+1)), _d.frames]);
				job.setPrivateData('last.message', "Number of pages in PDF does not match number of columns in die. PDF: " + (numPages / 2) + ", die: " + (_d.x * _d.y * (_d.frames + 1)) + ", frames: " + _d.frames);
				job.setPrivateData('last.severity', "3");
				job.sendToData(3, job.getPath() );
				return;
			}
		} else if (numPages != 2) {
			job.log(3, "PDF has just one page");
			job.setPrivateData('last.message', "PDF has just one page");
			job.setPrivateData('last.severity', "3");
			job.sendToData(3, job.getPath() );
			return;
		}
		
		job.log(-1, "die is valid: %1" , _d.valid);
		
		if (_d.valid) {
			// define needed private variables
			_d.SetPrivateVariables(job);
			var dieDS = job.createDataset('Opaque');
			
			if (s.download(_d.path, dieDS.getPath() )) {
				job.setPrivateData('last.message', "properties for die " + _d.number + " read");
				job.setPrivateData('last.severity', "1");
				
				//save original filename
				job.setPrivateData('swap.name.original', job.getNameProper() );
				job.setPrivateData('swap.extension.original', job.getExtension() );
				
				job.setDataset('diePDF', dieDS );
				job.sendToData(1, job.getPath() );
			} else {
				job.log(3, "PDF for die tool could not get downloaded from %1", _d.path);
				job.setPrivateData('last.message', "PDF for die tool could not get downloaded from %1" + _d.path);
				job.setPrivateData('last.severity', "3");
				job.sendToData(3, job.getPath() );
			}
	
		} else {
			job.log(3, _d.message);
			job.setPrivateData('last.message', _d.message );
			job.setPrivateData('last.severity', "3");
			job.sendToData(3,job.getPath() );
		}
	}
	catch (e) {
		job.fail(e);
	}
	
}

// Is invoked at regular intervals regardless of whether a new job arrived or not.
// The interval can be modified with s.setTimerInterval().
function timerFired( s : Switch )
{
}

class die {
		var x = 0;
		var y = 0;
		var a1 = 0;
		var a2 = 0;
		var l  = 0;
		var b = 0;
		var wr = 0; //default - Wortanfang
		var bb = 0;
		var path = "";
		var number = 0;
		var frames = 0;
		var textOffsetHorizontal = 0;
		var slotIsLandscape = false;
		var slotGetsRotaded; // will get set to ether true or false when calculation the rotation character for imposition
		var rotationCharacter; //used for pdfToolbox imposition and is set to t, l, r or b
		var MaterialWidth = 331;
		var valid = true;
		var numberOfPages = 2; //default: print PDF and die as separate pages
		var message = "";
		var job;
		var s;
		var ignoreOrientation = false; //if l = b, orientation does not matter
		
		function die ( XML : Document, isPortrait, _s, _job ) {
			s = _s;
			job = _job;
			number = XML.evalToNumber('//die/nummer'); // Stanzennummer
			valid = (valid && (!isNaN (number)));
			
			x = XML.evalToNumber('//die/x'); // Anzahl der Werkzeuge in X-Richtung
			valid = (valid && (!isNaN (x)));
			y = XML.evalToNumber('//die/y'); // Anzahl der Werkzeuge in >-Richtung
			valid = (valid && (!isNaN (y)));
			b = XML.evalToNumber('//die/b'); // Breite des Werkzeuges
			valid = (valid && (!isNaN (b)));
			l = XML.evalToNumber('//die/l'); // Höhe des Werkzeuges
			valid = (valid && (!isNaN (l)));
			a1 = XML.evalToNumber('//die/a1'); // X-Abstand zwischen den einzenen Nutzen
			valid = (valid && (!isNaN (a1)));
			a2 = XML.evalToNumber('//die/a2'); // Y-Abstand zwischen den einzenen Nutzen
			valid = (valid && (!isNaN (a2)));
			bb = XML.evalToNumber('//job/bahnbreite'); // Bahnbreite
			valid = (valid && (!isNaN (bb)));
			frames = XML.evalToNumber('count(/job/bahnen)');
			valid = (valid && (!isNaN (frames)));
			
			ignoreOrientation = (l == b);
			
			if (! valid) {
				message = "XML returned for die number " + number + " is not valid";
				return;
			}
			
			MaterialWidth = XML.evalToNumber('/job/bahnbreite_max');
			valid = (valid && ((MaterialWidth >= materialWidth.min) && (MaterialWidth <= materialWidth.max)));
			if (! valid) {
				 message = "Material width (" + MaterialWidth + ") is not within the range of " +materialWidth.min + " and " + materialWidth.max + "mm";
				 return;
			}
			
			wr = XML.evalToNumber('/job/wickelrichtung');
			
			valid = (valid && ((wr >= 1) && (wr <= 8)));
			if (!valid) {
				message =  "Wickelrichtung ist ungültig: " + wr;
				return;
			}
			
			
			//remove any newline characters
			path = XML.evalToString('//die/file').replace(/\\r|\\n/i,"");
			valid = (valid && (path.match(/https?:\/\/.*/) != undefined));
			if (! valid) {
				 message = "Path to file does not start with http: " + path;
				 return;
			}
			//to calcluate the rotation character, we need to know if the tool is portrait or landscape
			slotIsLandscape = (b > l);
			rotationCharacter = getRotationCharacter(isPortrait);
			
			valid = (valid && (typeof rotationCharacter != "undefined"));
			if (! valid) {
				 // message got set in function
				 return;
			}
			
			valid = setTextOffset();
			if (! valid) {
				 // message got set in function
				 return;
			}
			
			/*
			  // not yet active, maybe not even necessary
			  valid = cMinimalNeededSlug();
			if (! valid) {
				 // message got set in function
				 return;
			}
			  
			  */
		}

		// sets and checks if text offset has a positive value. A negative value would result in a pdfToolbox Error
		function setTextOffset () {
			textOffsetHorizontal = ((bb - (b * x) - (a1 * ( x-1)))/2) - cTextBoxDistanceReightTopHorizontal;
			if (textOffsetHorizontal >= 0) {
				return true; 
			} else {
			 message = 'Textoffset must be larger than or equal to 0';
			 return false;
		 }
		}
		
		// sets and checks if text offset has a positive value. A negative value would result in a pdfToolbox Error
		function cMinimalNeededSlug () {
			mindWidth = ((bb - (b * x) - (a1 * ( x-1)))/2) - cMinimalNeededBleed;
			if (mindWidth >= 0) {
				return true; 
			} else {
				message = "Minimal needed with is too small";
			 return false;
		 }
		}
		
		// Height of Sheet has to be calculated from height (l) of slot plus distance (a2) between slots
		function getSheetHeight () : Float {
			return (y * l + y * a2);
		}
		
		// This is the width of the die tool, but not the imposed sheed
		function getWidth () : Float {
			return (x * b + x * a2);
		}
		
		// Width of Sheet if stored in bb and does not get calulated by the slot width
		function getSheetWidth() : Float {
			return bb;
		}
		
		// for pdfToolbox impose, the slot needs to be retated it the the positioning in the imposition schema is l or r
		function getSlotWidth () : Float {
			return ((slotGetsRotaded) ? l : b);
		}
		
		// for pdfToolbox impose, the slot needs to be retated it the the positioning in the imposition schema is l or r
		function getSlotHeight () : Float {
			return ((slotGetsRotaded) ? b : l);
		}
		
		function SetPrivateVariables (job) {
			job.setPrivateData('die.rows',y);
			job.setPrivateData('die.columns',x);
			job.setPrivateData('die.offcutLeft',getOffcut("left"));
			job.setPrivateData('die.offcutReight',getOffcut("reight"));
			job.setPrivateData('die.offcutTop',getOffcut("top"));
			job.setPrivateData('die.offcutBottom',getOffcut("bottom"));
			job.setPrivateData('die.bleedLeft',getBleed("left"));
			job.setPrivateData('die.bleedReight',getBleed("reight"));
			job.setPrivateData('die.bleedTop',getBleed("top"));
			job.setPrivateData('die.bleedBottom',getBleed("bottom"));
			job.setPrivateData('die.slotWidth',getSlotWidth());
			job.setPrivateData('die.slotHeight',getSlotHeight());
			job.setPrivateData('die.sheetWidth',getSheetWidth());
			job.setPrivateData('die.sheetHeight',getSheetHeight());
			job.setPrivateData('die.customerImpositionSchema',getCustomImpositionSchema());
			job.setPrivateData('die.toolOriginX' , getToolOrigin("x"));
			job.setPrivateData('die.toolOriginY' , getToolOrigin("y"));
			job.setPrivateData('die.b' , b);
			job.setPrivateData('die.l' , l);
			job.setPrivateData('die.mediaStartOffset' , getMediaStartOffset());
			job.setPrivateData('die.maxSheetLength' , materialLength);
			job.setPrivateData('die.textOffsetHorizontal' , textOffsetHorizontal);
			job.setPrivateData('die.numberOfFrames' , frames );
						
		}
		
		//return the direction of the head of the PDF in as character t, l, r or b, depending in the orientation of the original PDF
		function getRotationCharacter(isPdfPortrait : Boolean) : string {
			
			// if ignoreOrientation then isPdfPortrait is always true
			if (!((wr >= 1) && (wr <= 8)))
				throw ('Wickelrichtung ist nicht definiert');
			
			if (typeof rotationCharacter == "string")
				return rotationCharacter;
				else {
				switch (wr) {
				case 3:
				case 7:
					slotGetsRotaded = true;
					// Wortanfang, innen
					// Wortanfang, außen
					if (isPdfPortrait) {
						if (slotIsLandscape || ignoreOrientation)
							 return 'r';
						else
							message = 'Tool requires a landscape PDF';
							
					} else {
						if (slotIsLandscape)
							message = 'Landscape PDF cannot be used with this winding direction for landscape tool';
						else
							return 'r';
					}	
					break;
				case 4:
				case 8:
					slotGetsRotaded = true;
					// Wortende, innen
					// Wortanfang, außen
					if (isPdfPortrait) {
						if (slotIsLandscape || ignoreOrientation)
							return 'l';
						else
							message = 'Tool requires a landscape PDF';
					} else {
						if (slotIsLandscape)
							message = 'Landscape PDF cannot be used with this winding direction for landscape tool';
						else
							return 'l';
					}	
					break;
				case 2:
				case 6:
					slotGetsRotaded = false;
					// Fuß voraus, innen
					// Kopf voraus, außen
					if (isPdfPortrait) {
						if (!slotIsLandscape || ignoreOrientation)
							return 't';
						else
							message = 'Portrait PDF cannot be used with this winding direction for landscape tool';
					} else {
						if (slotIsLandscape)
							return 't';
						else
							message = 'Tool requires a landscape PDF';
							
					}	
					break;
				case 1:
				case 5:
					slotGetsRotaded = false;
					// Kopf voraus
					// Fuß voraus, außen
					if (isPdfPortrait) {
						if (!slotIsLandscape || ignoreOrientation)
							return 'b';
						else
							message = 'Portrait PDF cannot be used with this winding direction for landscape tool';
					} else {
						if (slotIsLandscape)
							return 'b';
						else							
							message = 'Tool requires a landscape PDF';
					}	
					break;
				default:
					throw ("wr not known: " + wr);
				}
			}
			return undefined;
		}
		
		function getToolOrigin( _axis ) : real {
			//get x or y origin of die Tool relative to MediaBox Origin
			var _retval = 0;
			if (_axis == "x") {
				// return the position of the first slot
				// use the real slot width, not the rotated
				_retval = ((getSheetWidth()  - (x * b) - ( (x-1) * a1)) / 2);
				
			} else {
				// return half of the a2 distance
				_retval = a2 / 2;
			}
			
			return _retval;
		}
		
		function getBleed(theEdge) : real {
			
			//slots might get rotated!!
			var _bleed = 0;
			switch (theEdge) {
			case "left":
			case "reight":
				if (slotGetsRotaded)
					_bleed = a2 / 2;
				else
					_bleed = a1 / 2;
				break;
			default:
				if (slotGetsRotaded)
					_bleed = a1 / 2;
				else
					_bleed = a2 / 2;
			}
			//return 3mm of bleed of less if a1 or a2 is less
			return (Math.min( cBleedMaxMm, _bleed ));
			
		}
		
		function getOffcut(theEdge) : real {
			//slots might get rotated!!			
			// add half of the x or y distance between the slots (tools)
			switch (theEdge) {
			case "left":
			case "reight":
				// do not rotate at the moment
				if (slotGetsRotaded)
					return a2 / 2;
				else
					return a1 / 2;
				break;
			default:
				if (slotGetsRotaded)
					return a1 / 2;
				else
					return a2 / 2;
				break;
			}
		}
		
		function getMediaStartOffset() : real {
			// get x Offset of printed Media
			// assumtion: everything is centered
			var _retval = 0;
			_retval = (getSheetWidth() - MaterialWidth) / 2;
			
			return _retval;
		}
		
		//the PDF gets placed Step and Repeat
		//ToDo: if PDF has more than one page, then the number of page must match the value of x.
		function getCustomImpositionSchema () : String {
			var retval = "";
			if (typeof rotationCharacter == "undefined")
				throw ("Rotationcharacter is not yet defined. Call setRotationCharacter(<PDForitentation>) first");
			for (var _y = 0; _y < y; _y++) {
				//iterate through number of rows
				for (var _x = 0; _x < x; _x++) {
					//iterate through number of columnes
					//use page number if PDF has multiple pages, in steps of 2
					//add \t between parameters of on row
					// add \n when a new line is needed
					retval += ((numberOfPages == 2) ? "1" :  ( 1 + (_y * x * 1) + (_x * 1)).toString()) + rotationCharacter + ((_x < (x-1)) ? '\t' : ((_y < y) ? '\n' : ""));
					job.log(-1, "x: %1", _x);
					job.log(-1, "y: %1", _y);
					job.log(-1, "retval: %1", retval);
				}
			}
			return retval;
		}
		
	}

